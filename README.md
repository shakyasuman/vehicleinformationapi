# README #

Different automobile vendors may be seeking for accessing vehicle information from a centralized database through APIs
in order to integrate it to their own web application.

The main concept of Automobile Datastore API is to provide a platform for automobile companies to know the status details of vehicle.
It is basically a full stack project but mainly focused in the Restful web service. It includes two parts - Restful web service and web application.

The Restful Api service exposes different endpoints to control over the data. The vehicle information has following properties:
* Vin Number  -   Vehicle Indentication Number is the identifying code for a SPECIFIC automobile
* Make        -   Brand of vehicle like Ford, Hyundai, Polo etc
* Year        -   Make year of vehicle
* Model       -   Release model year
* Status      -   The status of vehicle can be Production, Marketing, Sold, Returned and NotAvailable.

The web application provides you the user interface to carry out the different actions provided by the Restful web service. It is an
Admin interface through which you can do get, create, update and delete operations.

### Version ###
1.0.0

### Requirements ###
* Maven >= 3.3
* JAVA >= 1.8
* Postgres >= 9.2
* Tomcat Server or Any Application Server

### VehicleInformation Datasotre API ###
This is a RESTFul web service built using Jersey library. DAO Design Pattern is followed while implementing the project in order to
abstract and encapsulate the implementation details. JAX-RS/Jersey is a standard that makes it easy to create a RESTful service that
can be deployed to any Java application server: GlassFish, WebLogic, WebSphere, JBoss, etc.

## Endpoints

| Path           	| Method 	| Description                                                      	| Parameters                                                                                                                                                                                                                                                                             	|
|----------------	|--------	|------------------------------------------------------------------	|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| /getData       	| GET    	| Returns all vehicle information                                  	|                                                                                                                                                                                                                                                                                        	|
| /getData/query 	| GET    	| Returns vehicle information that is filtered by given parameters 	| Pass value to filter in parameters - vinNumber, make,  model, year and status                                                                                                                                                                                                          	|
| /save          	| POST   	| Saves new vehicle information                                    	| Pass the given json int request body { 	vinNumber:Unique AlphaNumeric Value, 	make: Any String Value, 	model: Number Value, 	year: Number Value,         status: one of the value in the give list [Production, Marketing, Sold,Returned, NotAvailable] }                                  	|
| /update        	| PUT    	| Update existing vehicle information                              	| Pass the value of vinNumber in request and  json body for updated one { 	vinNumber:Unique AlphaNumeric Value, 	make: Any String Value, 	model: Number Value, 	year: Number Value,         status: one of the value in the give list [Production, Marketing, Sold,Returned, NotAvailable] } 	|
| /delete        	| DELETE 	| Delete vehicle information                                       	| Pass the value of vinNumber in request                                                                                                                                                                                                                                                 	|

## Examples

**Example 1**: Get every available vehicle information

**Endpoint**: https://automobilefyi.herokuapp.com/api/getData
 
**Result**: 

```json
{"result":[{"vinNumber":"A001341","make":"Ford","year":2011,"model":2011,"status":"Sold"},{"vinNumber":"A0013413243","make":"Ford","year":2011,"model":2011,"status":"Returned"},{"vinNumber":"A00133434","make":"Hyundai","year":2011,"model":2011,"status":"Production"},{"vinNumber":"A00343422","make":"Ford","year":2012,"model":2012,"status":"NotAvailable"}]}
```
**Example 2**: Get Ford vehicle only

**Endpoint**: https://automobilefyi.herokuapp.com/api/getData/query?make=Ford
 
**Result**: 

```json
{"result":[{"vinNumber":"asdf234","make":"Ford","year":2012,"model":2012,"status":"Production"},{"vinNumber":"100ABC","make":"FORD","year":2011,"model":2011,"status":"Returned"}]}
```

### VehicleInformation Datasotre API Management ###
This is a web application built with Spring MVC framework in order to do get, filter, create, update and delete operations through user interface. MVC pattern is strictly followed in the project to seperate out the frontend and backend logic.

## Application URL
(https://automobilefyi.herokuapp.com/)

## Preview

![Home Preview](preview/home_page.png)
![Add New Vehicle Info Preview](preview/add_new_info.png)
![Edit Existing Vehicle Info Preview](preview/edit_vehicle_info.png)

### Personal Contact ###
**LinkedIn Profile:** [Profile](https://www.linkedin.com/in/suman-shakya-7959a683/)


