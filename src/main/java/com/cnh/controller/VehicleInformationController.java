package com.cnh.controller;

import java.util.List;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cnh.dao.VehicleInformationServiceDao;
import com.cnh.daoImpl.VehicleInformationServiceImpl;
import com.cnh.entity.VehicleInformation;

/**
 * The controller handles the request/response action of UI
 */
@Controller
public class VehicleInformationController {
	VehicleInformationServiceDao vehicleInformationServiceDao = new VehicleInformationServiceImpl();
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index() {
		List<VehicleInformation> data = vehicleInformationServiceDao.getData("getData");
		ModelAndView mv = new ModelAndView();
		mv.addObject("data", data);
		mv.addObject("vehicleInformation", new VehicleInformation());
		mv.setViewName("home");
		return mv;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ModelAndView create(@ModelAttribute("vehicleInformation") VehicleInformation vehicleInformation,
			BindingResult result, Model model) {
		JSONObject responseObj = vehicleInformationServiceDao.putPostData(vehicleInformation, "POST");
		List<VehicleInformation> data = vehicleInformationServiceDao.getData("getData");
		ModelAndView mv = new ModelAndView();
		mv.setViewName("home");
		mv.addObject("data", data);
		mv.addObject("msg", (responseObj != null && responseObj.has("message")) ? responseObj.get("message")
				: "Error occurred while saving. Please view server logs.");
		return mv;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public @ResponseBody JSONObject edit(@RequestParam("vinNumber") String vinNumber) {
		List<VehicleInformation> data = vehicleInformationServiceDao.getData("getData/query?vinNumber=" + vinNumber);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status", "success");
		jsonObject.put("vehicleInformation", data.get(0));
		return jsonObject;
	}

	@RequestMapping(value = "/saveEdit", method = RequestMethod.POST)
	public ModelAndView saveEdit(@ModelAttribute("vehicleInformation") VehicleInformation vehicleInformation,
			BindingResult result, Model model) {
		JSONObject responseObj = vehicleInformationServiceDao.putPostData(vehicleInformation, "PUT");
		List<VehicleInformation> data = vehicleInformationServiceDao.getData("getData");
		ModelAndView mv = new ModelAndView();
		mv.setViewName("home");
		mv.addObject("data", data);
		mv.addObject("msg", responseObj.has("message") ? responseObj.get("message")
				: "Error occurred while saving. Please view server logs.");
		return mv;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam("vinNumber") String vinNumber) {
		JSONObject responseObj = vehicleInformationServiceDao.deleteData(vinNumber);
		List<VehicleInformation> data = vehicleInformationServiceDao.getData("getData");
		ModelAndView mv = new ModelAndView();
		mv.setViewName("home");
		mv.addObject("data", data);
		mv.addObject("vehicleInformation", new VehicleInformation());
		mv.addObject("msg", (responseObj != null && responseObj.has("message")) ? responseObj.get("message")
				: "Error occurred while saving. Please view server logs.");
		return mv;
	}

	@RequestMapping("/filter")
	public ModelAndView filter() {
		return new ModelAndView("home", "vehicleInformation", new VehicleInformation());
	}
}
