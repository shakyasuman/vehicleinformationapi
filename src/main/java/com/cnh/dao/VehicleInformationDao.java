package com.cnh.dao;

import com.cnh.entity.VehicleInformation;

import java.util.List;

/**
 *
 * This is a Data Access Object (DAO). This interface defines the standard operations to be performed on a model object(s) {@link VehicleInformation}.
 */
public interface VehicleInformationDao {
    /**
     * @param vinNumber some alphanumeric values representing as Vehicle Identification Number.
     * @return an instance of {@link VehicleInformation} when value of {@param vinNumber} is matched.
     */
    VehicleInformation findByVinNumber(String vinNumber);

    /**
     * @param make vehicle's manufacturing company name e.g Ford
     * @return list of instance {@link VehicleInformation} when value of {@param make} is matched.
     */
    List<VehicleInformation> findByMake(String make);

    /**
     * @param year vehicle's make year
     * @return list of instance {@link VehicleInformation} when value of {@param year} is matched.
     */
    List<VehicleInformation> findByYear(Integer year);

    /**
     * @param model model of vehicle
     * @return list of instance {@link VehicleInformation} when value of {@param model} is matched.
     */
    List<VehicleInformation> findByModel(Integer model);

    /**
     * @return list of vehicle meta data from the database
     */
    List<VehicleInformation> getAll();

    /**
     * @param vehicleInformation an instance of {@link VehicleInformation} to be saved
     */
    void save(VehicleInformation vehicleInformation);

    /**
     * @param VehicleInformationToBeEdited an instance of {@link VehicleInformation} to be edited
     * @param newVehicleInformation the new data model to be updated in {@param vehicleInformation}
     */
    void update(VehicleInformation VehicleInformationToBeEdited, VehicleInformation newVehicleInformation);


    /**
     * @param vehicleInformation an instance of {@link VehicleInformation} to be deleted
     */
    void delete(VehicleInformation vehicleInformation);
}
