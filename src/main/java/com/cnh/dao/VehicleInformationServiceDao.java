package com.cnh.dao;

import com.cnh.entity.VehicleInformation;

import java.util.List;

import org.json.JSONObject;

/**
 *
 * This is a Data Access Object (DAO). This interface defines the standard operations to be performed while calling APIs.
 */
public interface VehicleInformationServiceDao {
    /**
     * @param endPoint for get request.
     * @return list of {@link VehicleInformation}.
     */
    List<VehicleInformation> getData(String endPoint);
    
    /**
     * @param vinNumber.
     * @return JSONObject response details.
     */
    JSONObject deleteData(String vinNumber);
    
    /**
     * @param vehicleInformation an instance to be added/updated
     * @param type POST/PUT
     * @return JSONObject response details.
     */
    JSONObject putPostData(VehicleInformation vehicleInformation, String type);

    
}
