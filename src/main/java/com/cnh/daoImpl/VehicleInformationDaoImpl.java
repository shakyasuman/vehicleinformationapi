package com.cnh.daoImpl;

import com.cnh.dao.VehicleInformationDao;
import com.cnh.entity.VehicleInformation;
import com.cnh.enums.Status;
import com.cnh.jdbc.DatabaseConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * {@inheritDoc}
 *
 * This class implements all the methods of the DAO class {@link VehicleInformationDao}
 */
public class VehicleInformationDaoImpl implements VehicleInformationDao {
    private Connection connection;

    public VehicleInformationDaoImpl() { }

    @Override
    public VehicleInformation findByVinNumber(final String vinNumber) {
    	VehicleInformation vehicleInformation = null;
    	try {
			connection = (Connection) DatabaseConnection.getInstance().getConnection();
        	PreparedStatement statement = (PreparedStatement) connection.prepareStatement("SELECT * FROM "
        			+ "vehicle_information WHERE vin_number = ? LIMIT 1;");    
        	statement.setString(1, vinNumber);    
        	ResultSet rs = statement.executeQuery();
        	while(rs.next()){
              vehicleInformation = new VehicleInformation(rs.getString("vin_number"),
            		  rs.getString("make"), rs.getInt("year"), rs.getInt("model"), Status.valueOf(rs.getString("status")));
             }
             rs.close();
             statement.close();
             connection.close();
    	} catch (SQLException e) {
    		System.out.print("SQL exception occur while selecting data by vinNumber; msg ="+e.getMessage());
		}
        return vehicleInformation;
    }

    @Override
    public List<VehicleInformation> findByMake(String make) {
    	List<VehicleInformation> vehicleInformationList = new ArrayList<>();
    	try {
			connection = (Connection) DatabaseConnection.getInstance().getConnection();
            PreparedStatement statement = (PreparedStatement) connection.prepareStatement("SELECT * FROM vehicle_information WHERE make = ?;");    
        	statement.setString(1, make);    
        	ResultSet rs = statement.executeQuery();
        	while(rs.next()){
              vehicleInformationList.add(new VehicleInformation(rs.getString("vin_number"),
            		  rs.getString("make"), rs.getInt("year"), rs.getInt("model"), Status.valueOf(rs.getString("status"))));
             }
             rs.close();
             statement.close();
             connection.close();
    	} catch (SQLException e) {
    		System.out.print("SQL exception occur while selecting data by make; msg ="+e.getMessage());
		}
        return vehicleInformationList;
    }

    @Override
    public List<VehicleInformation> findByYear(Integer year) {
    	List<VehicleInformation> vehicleInformationList = new ArrayList<>();
    	try {
			connection = (Connection) DatabaseConnection.getInstance().getConnection();
            PreparedStatement statement = (PreparedStatement) connection.prepareStatement("SELECT * FROM vehicle_information WHERE year = ?;");    
        	statement.setInt(1, year);    
        	ResultSet rs = statement.executeQuery();
        	while(rs.next()){
              vehicleInformationList.add(new VehicleInformation(rs.getString("vin_number"),
            		  rs.getString("make"), rs.getInt("year"), rs.getInt("model"), Status.valueOf(rs.getString("status"))));
             }
             rs.close();
             statement.close();
             connection.close();
    	} catch (SQLException e) {
    		System.out.print("SQL exception occur while selecting data by year; msg ="+e.getMessage());
		}
        return vehicleInformationList;
    }

    @Override
	public List<VehicleInformation> findByModel(Integer model) {
    	List<VehicleInformation> vehicleInformationList = new ArrayList<>();
    	try {
			connection = (Connection) DatabaseConnection.getInstance().getConnection();
        	Statement statement = (Statement) connection.createStatement();
        	String sql = "SELECT * FROM vehicle_information WHERE model = "+model;
            ResultSet rs = statement.executeQuery(sql);
        	while(rs.next()){
              vehicleInformationList.add(new VehicleInformation(rs.getString("vin_number"),
            		  rs.getString("make"), rs.getInt("year"), rs.getInt("model"), Status.valueOf(rs.getString("status"))));
             }
             rs.close();
             statement.close();
             connection.close();
    	} catch (SQLException e) {
    		System.out.print("SQL exception occur while selecting data by model; msg ="+e.getMessage());
		}
        return vehicleInformationList;
    }

    @Override
    public List<VehicleInformation> getAll() {
    	List<VehicleInformation> vehicleInformationList = new ArrayList<>();
    	try {
			connection = (Connection) DatabaseConnection.getInstance().getConnection();
        	Statement statement = (Statement) connection.createStatement();
        	String sql = "SELECT * FROM vehicle_information";
            ResultSet rs = statement.executeQuery(sql);
        	while(rs.next()){
              vehicleInformationList.add(new VehicleInformation(rs.getString("vin_number"),
            		  rs.getString("make"), rs.getInt("year"), rs.getInt("model"), Status.valueOf(rs.getString("status"))));
             }
             rs.close();
             statement.close();
             connection.close();
    	} catch (SQLException e) {
    		System.out.print("SQL exception occur while selecting data; msg ="+e.getMessage());
		}
        return vehicleInformationList;
    }

    @Override
    public void save(VehicleInformation vehicleInformation) {
    	try {
			connection = (Connection) DatabaseConnection.getInstance().getConnection();
    		PreparedStatement stmt=(PreparedStatement) connection.prepareStatement("INSERT INTO vehicle_information(vin_number,"
        			+ "make, year, model, status) values(?,?,?,?,?)");  
        	stmt.setString(1, vehicleInformation.getVinNumber());
        	stmt.setString(2,vehicleInformation.getMake());
        	stmt.setInt(3, vehicleInformation.getYear());
        	stmt.setInt(4, vehicleInformation.getModel());
        	stmt.setString(5, vehicleInformation.getStatus().toString());
        	stmt.executeUpdate();  
        	connection.close();  
    	} catch (SQLException e) {
    		System.out.print("SQL exception occur while inserting data; msg ="+e.getMessage());
		}
    }

    @Override
    public void update(VehicleInformation vehicleInformationToBeEdited, VehicleInformation newVehicleInformation) {
    	try {
			connection = (Connection) DatabaseConnection.getInstance().getConnection();
    		PreparedStatement stmt=(PreparedStatement) connection.prepareStatement("UPDATE vehicle_information SET vin_number=?, "
        			+ "make=?, year=?, model=?, status=? WHERE vin_number=?;");  
        	stmt.setString(1, newVehicleInformation.getVinNumber());
        	stmt.setString(2,newVehicleInformation.getMake());
        	stmt.setInt(3, newVehicleInformation.getYear());
        	stmt.setInt(4, newVehicleInformation.getModel());
        	stmt.setString(5, newVehicleInformation.getStatus().toString());
        	stmt.setString(6, vehicleInformationToBeEdited.getVinNumber());
        	stmt.executeUpdate();  
        	connection.close();  
    	} catch (SQLException e) {
    		System.out.format("SQL exception occur while udpating data for vinNumber = %s; msg %s",
    				vehicleInformationToBeEdited.getVinNumber(),e.getMessage());
		}
    }

    @Override
    public void delete(VehicleInformation vehicleInformation) {
    	try {
			connection = (Connection) DatabaseConnection.getInstance().getConnection();
    		PreparedStatement stmt=(PreparedStatement) connection.prepareStatement("DELETE FROM vehicle_information WHERE vin_number=?;");  
        	stmt.setString(1, vehicleInformation.getVinNumber());
        	stmt.executeUpdate();  
        	connection.close(); 
    	} catch (SQLException e) {
			System.out.format("SQL exception occur while deleting data for vinNumber = %s; msg %s",
    				vehicleInformation.getVinNumber(),e.getMessage());
		}
    	
    }
}
