package com.cnh.daoImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cnh.dao.VehicleInformationServiceDao;
import com.cnh.entity.VehicleInformation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class VehicleInformationServiceImpl implements VehicleInformationServiceDao{
	private static HttpURLConnection con;
	private static String host = "https://automobilefyi.herokuapp.com/";
	private static String baseURI = host + "api/";
	
	@Override
	public List<VehicleInformation> getData(String endPoint) {
		List<VehicleInformation> data = new ArrayList<VehicleInformation>();
		try {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			URL myurl = new URL(baseURI + endPoint);
			con = (HttpURLConnection) myurl.openConnection();
			con.setRequestMethod("GET");
			StringBuilder content;
			try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
				String line;
				content = new StringBuilder();
				while ((line = in.readLine()) != null) {
					content.append(line);
					content.append(System.lineSeparator());
				}
			}
			try {
				JSONObject jsonObject = new JSONObject(content.toString());
				if (jsonObject.has("result")) {
					JSONArray list = (JSONArray) jsonObject.get("result");
					for (int i = 0; i < list.length(); i++) {
						VehicleInformation vehicleInformation = gson.fromJson(list.get(i).toString(),
								VehicleInformation.class);
						data.add(vehicleInformation);
					}
				} else {
					System.out.println("Error Occurred!!!");
				}
			} catch (JSONException err) {
				System.out.print(err.getMessage());
			}
		} catch (Exception e) {
			System.out.println("Exception occurred while getting data. msg=" + e.getMessage());
		} finally {
			con.disconnect();
		}
		return data;
	}

	@Override
	public JSONObject deleteData(String vinNumber) {
		try {
			URL myurl = new URL(baseURI + "delete?vinNumber=" + vinNumber);
			con = (HttpURLConnection) myurl.openConnection();
			con.setRequestMethod("DELETE");
			con.setUseCaches(false);
			con.setConnectTimeout(50000);
			con.setReadTimeout(50000);
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Host", host);
			StringBuilder content;
			int responseCode = con.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_NO_CONTENT) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("message", "Successfully Deleted.");
				return jsonObject;
			}
			try (BufferedReader in = new BufferedReader(new InputStreamReader(
					responseCode == HttpURLConnection.HTTP_OK ? con.getInputStream() : con.getErrorStream()))) {
				String line;
				content = new StringBuilder();
				while ((line = in.readLine()) != null) {
					content.append(line);
					content.append(System.lineSeparator());
				}
				JSONObject jsonObject = new JSONObject(content.toString());
				return jsonObject;
			} catch (JSONException e) {
				System.out.println("JSON Error while deleting data. msg=" + e.getMessage());
			}
		} catch (Exception e) {
        	System.out.println("Exception Occurred while deleting data. msg="+e.getMessage());
		} finally {
			con.disconnect();
		}
		return null;
	}

	@Override
	public JSONObject putPostData(VehicleInformation vehicleInformation, String type) {
		Gson gson = new Gson();
		try {
			URL url = new URL(
					baseURI + (type == "POST" ? "save" : "update?vinNumber=" + vehicleInformation.getVinNumber()));
			con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod(type);
			con.setUseCaches(false);
			con.setConnectTimeout(50000);
			con.setReadTimeout(50000);
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Host", host);
			con.connect();
			OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
			out.write(gson.toJson(vehicleInformation));
			out.close();
			StringBuilder content;
			int responseCode = con.getResponseCode();
			try (BufferedReader in = new BufferedReader(new InputStreamReader(
					responseCode == HttpURLConnection.HTTP_OK ? con.getInputStream() : con.getErrorStream()))) {
				String line;
				content = new StringBuilder();
				while ((line = in.readLine()) != null) {
					content.append(line);
					content.append(System.lineSeparator());
				}
				JSONObject jsonObject = new JSONObject(content.toString());
				return jsonObject;
			} catch (JSONException e) {
				System.out.println("Exception occured while adding/updating data. msg=" + e.getMessage());
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				con.disconnect();
		}
		return null;
	}

}
