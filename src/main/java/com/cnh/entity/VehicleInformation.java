package com.cnh.entity;

import com.cnh.enums.Status;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class VehicleInformation {
    /**
     * {@link #vinNumber some alphanumeric values representing as Vehicle Identification Number.}
     * {@link #make vehicle's manufacturing company name e.g Ford}
     * {@link #year vehicle's make year}
     * {@link #model model of vehicle}
     * {@link #status Status of vehicle}
     */
    private String vinNumber;
    private String make;
    private Integer year;
    private Integer model;
    private Status status;

    public VehicleInformation() { }

    public VehicleInformation(String vinNumber, String make, Integer year, Integer model, Status status) {
        this.vinNumber = vinNumber;
        this.make = make;
        this.year = year;
        this.model = model;
        this.status = status;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber=vinNumber;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make=make;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year=year;
    }

    public Integer getModel() {
        return model;
    }

    public void setModel(Integer model) {
        this.model=model;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status=status;
    }

    @Override
    public String toString() {
        return "VehicleInformation [vinNumber=" + vinNumber + ", make=" + make + ", year=" + year + ", model=" + model + "]";
    }
}
