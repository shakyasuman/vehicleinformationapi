package com.cnh.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * This contains possible status of vehicle.
 */
public enum Status {
    Production,
    Marketing,
    Sold,
    Returned,
    NotAvailable;

    private Status() {
    }

    public static List<Status> getStatusList() {
        List<Status> statuses =
                new ArrayList<Status>(EnumSet.allOf(Status.class));
        return statuses;

    }

    public static Status fromString(String text) {
        for (Status status : Status.values()) {
            if (status.toString().equalsIgnoreCase(text)) {
                return status;
            }
        }
        return null;
    }
}
