package com.cnh.exceptions;

public class ResponseSuccess extends Throwable{
    private static final long serialVersionUID = 1L;

    public ResponseSuccess(String msg){
        super(msg);
    }
}
