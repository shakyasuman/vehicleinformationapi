package com.cnh.exceptions;

import com.cnh.pojo.ResponseDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ResponseSuccessMapper extends Throwable implements ExceptionMapper<ResponseSuccess>
{
	private static final long serialVersionUID = 1L;

	@Override
    public Response toResponse(ResponseSuccess responseSuccess) {
        ResponseDetails responseDetails = new ResponseDetails();
        responseDetails.setCode("200");
        responseDetails.setStatus("Success");
        responseDetails.setMessage(responseSuccess.getMessage());
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String jsonObj = gson.toJson(responseDetails);
        return Response.status(Response.Status.OK).entity(jsonObj)
                .type(MediaType.APPLICATION_JSON).build();
    }
}

