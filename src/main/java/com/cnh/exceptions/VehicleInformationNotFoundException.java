package com.cnh.exceptions;

import com.cnh.pojo.ResponseDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class VehicleInformationNotFoundException extends Exception implements ExceptionMapper<VehicleInformationNotFoundException>
{
	private static final long serialVersionUID = 1L;

	@Override
    public Response toResponse(VehicleInformationNotFoundException exception)
    {
        ResponseDetails responseDetails = new ResponseDetails();
        responseDetails.setCode("404");
        responseDetails.setStatus("Not Found.");
        responseDetails.setMessage("Vehicle Information not found.");
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String jsonObj = gson.toJson(responseDetails);
        return Response.status(Response.Status.NOT_FOUND).entity(jsonObj)
                .type(MediaType.APPLICATION_JSON).build();
    }
}
