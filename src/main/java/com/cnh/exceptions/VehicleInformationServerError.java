package com.cnh.exceptions;

public class VehicleInformationServerError extends Throwable{
    private static final long serialVersionUID = 1L;

    public VehicleInformationServerError(String msg){
        super(msg);
    }
}





