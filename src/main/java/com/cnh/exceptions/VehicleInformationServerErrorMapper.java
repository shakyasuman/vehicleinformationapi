package com.cnh.exceptions;

import com.cnh.pojo.ResponseDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class VehicleInformationServerErrorMapper extends Throwable implements ExceptionMapper<VehicleInformationServerError>
{
	private static final long serialVersionUID = 1L;

	@Override
    public Response toResponse(VehicleInformationServerError vehicleInformationServerError) {
        ResponseDetails responseDetails = new ResponseDetails();
        responseDetails.setCode("500");
        responseDetails.setStatus("Internal Server Error.");
        responseDetails.setMessage(vehicleInformationServerError.getMessage());
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String jsonObj = gson.toJson(responseDetails);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(jsonObj)
                .type(MediaType.APPLICATION_JSON).build();
    }
}

