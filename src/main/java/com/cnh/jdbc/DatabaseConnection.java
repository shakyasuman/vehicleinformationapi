package com.cnh.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DatabaseConnection {

    private static DatabaseConnection instance;
    private Connection connection;
    private String url = "jdbc:postgresql://ec2-34-233-226-84.compute-1.amazonaws.com:5432/d1jd0s279vddct?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
    private String username = "tffrzlawslpfcy";
    private String password = "5d79d340b2ea27e0507fca76ab6d50ea9900a536a415de90b2caaf1eb381e5f7";

    private DatabaseConnection() throws SQLException {
        try {
            Class.forName("org.postgresql.Driver");
            this.connection = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException ex) {
            System.out.println("Database Connection Creation Failed : " + ex.getMessage());
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public static DatabaseConnection getInstance() throws SQLException {
        if (instance == null) {
            instance = new DatabaseConnection();
        } else if (instance.getConnection().isClosed()) {
            instance = new DatabaseConnection();
        }
        return instance;
    }
}
