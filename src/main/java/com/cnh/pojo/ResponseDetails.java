package com.cnh.pojo;

/**
 * This POJO class deals with handling the proper error messages.
 */
public class ResponseDetails {
    private String status;
    private String code;
    private String message;

    public String getStatus(){
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode(){
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage(){
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
