package com.cnh.service;

import com.cnh.dao.VehicleInformationDao;
import com.cnh.daoImpl.VehicleInformationDaoImpl;
import com.cnh.entity.VehicleInformation;
import com.cnh.enums.Status;
import com.cnh.exceptions.ResponseSuccess;
import com.cnh.exceptions.VehicleInformationNotFoundException;
import com.cnh.exceptions.VehicleInformationServerError;
import org.json.JSONException;

import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.regex.Pattern;

@Path("delete")
public class VehicleInformationDeleteApiService {
    private static VehicleInformationDao vehicleInformationDaoImpl = new VehicleInformationDaoImpl();

    /**
     *
     * An instance of {@link VehicleInformation} is removed from the list {@see VehicleInformationDaoImpl#vehicleInformationList}
     * @param vinNumber some alphanumeric value to represent as Vehicle Identification Number
     * @return list of available vehicles in {@see VehicleInformationDaoImpl#vehicleInformationList}
     * @throws JSONException
     * Execute with http://localhost:8080/VehicleInformationApi/api/delete?vinNumber=100ABC
     *
     */
    @DELETE
    @Path("")
    @Produces("application/json")
    public Response delete(@QueryParam("vinNumber") String vinNumber) throws JSONException, VehicleInformationNotFoundException, VehicleInformationServerError {
        try {
            Pattern p = Pattern.compile("\\S*(\\S*([a-zA-Z]\\S*[0-9])|([0-9]\\S*[a-zA-Z]))\\S*");
            if (vinNumber != null && p.matcher(vinNumber).matches()){
                VehicleInformation result = vehicleInformationDaoImpl.findByVinNumber(vinNumber);
                if (result != null && result.getStatus() != Status.Marketing && result.getStatus() != Status.Sold){
                    vehicleInformationDaoImpl.delete(result);
                } else if (result != null && result.getStatus() == Status.Marketing){
                    throw new VehicleInformationServerError("Marketing Vehicle cannot be deleted.");
                }else if (result != null && result.getStatus() == Status.Sold){
                    throw new VehicleInformationServerError("Sold Vehicle cannot be deleted.");
                }else {
                    throw new VehicleInformationNotFoundException();
                }
            } else if (vinNumber != null && !p.matcher(vinNumber).matches()) {
                throw new VehicleInformationServerError("Vin Number should only be alphanumeric.");
            }
            throw new ResponseSuccess("Successfully Deleted.");
        } catch (VehicleInformationNotFoundException ex){
            throw ex;
        } catch (VehicleInformationServerError ex){
            throw ex;
        }catch (Throwable t) {
            System.out.println(t.getMessage());
            return null;
        }
    }
}
