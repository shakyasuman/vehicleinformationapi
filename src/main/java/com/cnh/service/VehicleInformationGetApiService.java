package com.cnh.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;

import com.cnh.dao.VehicleInformationDao;
import com.cnh.daoImpl.VehicleInformationDaoImpl;
import com.cnh.entity.VehicleInformation;
import com.cnh.exceptions.VehicleInformationNotFoundException;
import com.cnh.exceptions.VehicleInformationServerError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * A Restful Web Service usually defines a URI, Uniform Resource Identifier a service, provides resource representation such as JSON and set of HTTP Methods.
 */
@Path("getData")
public class VehicleInformationGetApiService {
    private static VehicleInformationDao vehicleInformationDaoImpl = new VehicleInformationDaoImpl();

    /**
     *
     * @return list of vehicle information from {@see VehicleInformationDaoImpl#vehicleInformationList}
     * @throws JSONException
     * Execute with http://localhost:8080/VehicleInformationApi/api/getData
     *
     */
    @GET
    @Path("")
    @Produces("application/json")
    public Response getData() throws JSONException {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            Map<String, List<VehicleInformation>> returnDataMap = new HashMap<String, List<VehicleInformation>>();
            returnDataMap.put("result", new ArrayList<VehicleInformation>(vehicleInformationDaoImpl.getAll()));
            return Response.ok(gson.toJson(returnDataMap), MediaType.APPLICATION_JSON).build();
        }
        catch (Throwable t) {
            System.out.println(t.getMessage());
        }
        return null;
    }

    /**
     *
     * Query against the stored data {@see VehicleInformationDaoImpl#vehicleInformationList}
     * @param vinNumber some alphanumeric value to represent as Vehicle Identification Number
     * @param make vehicle's manufacturing company name e.g Ford
     * @param year vehicle's make year
     * @param model model of vehicle
     * @return list of {@link VehicleInformation} instances meeting the above params criteria
     * @throws JSONException
     * Execute with http://localhost:8080/VehicleInformationApi/api/vehicleInformation/query?vinNumber=2D4RN4DG6BR757892333
     *
     */
    @GET
    @Path("query")
    @Produces("application/json")
    public Response getData(@QueryParam("vinNumber") String vinNumber, @QueryParam("make") String make,
                            @QueryParam("year") String year, @QueryParam("model") String model) throws JSONException, VehicleInformationNotFoundException, VehicleInformationServerError {
        Map<String, VehicleInformation> vehicleInformationMap = new HashMap<>();
        try {
            Pattern p = Pattern.compile("\\S*(\\S*([a-zA-Z]\\S*[0-9])|([0-9]\\S*[a-zA-Z]))\\S*");
            if (vinNumber != null && p.matcher(vinNumber).matches()){
                VehicleInformation result = (VehicleInformation) vehicleInformationDaoImpl.findByVinNumber(vinNumber);
                if (result!=null && !vehicleInformationMap.containsKey(result.getVinNumber())){
                    vehicleInformationMap.put(result.getVinNumber(), result);
                } else if(result == null){
                    throw new VehicleInformationNotFoundException();
                }
            } else if (vinNumber != null && !p.matcher(vinNumber).matches()) {
                throw new VehicleInformationServerError("Vin Number should only be alphanumeric.");
            }
            if (make != null){
                List<VehicleInformation> result = vehicleInformationDaoImpl.findByMake(make);
                for(VehicleInformation obj:result) {
                	if (!vehicleInformationMap.containsKey(obj.getVinNumber())){
                        vehicleInformationMap.put(obj.getVinNumber(), obj);
                    }
                }
            }

            if (year != null && year.matches("^[0-9]*$")){
                List<VehicleInformation> result = vehicleInformationDaoImpl.findByYear(Integer.parseInt(year));
                for(VehicleInformation obj:result) {
                	if (!vehicleInformationMap.containsKey(obj.getVinNumber())){
                        vehicleInformationMap.put(obj.getVinNumber(), obj);
                    }
                }
            } else if (year != null && !year.matches("^[0-9]*$")) {
                throw new VehicleInformationServerError("Year should only be numbers.");
            }

            if (model != null && model.matches("^[0-9]*$")){
                List<VehicleInformation> result = vehicleInformationDaoImpl.findByModel(Integer.parseInt(model));
                for(VehicleInformation obj:result) {
                	if (!vehicleInformationMap.containsKey(obj.getVinNumber())){
                        vehicleInformationMap.put(obj.getVinNumber(), obj);
                    }
                }
            } else if (model != null && !model.matches("^[0-9]*$")) {
                throw new VehicleInformationServerError("Model should only be numbers.");
            }
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            Map<String, List<VehicleInformation>> returnDataMap = new HashMap<String, List<VehicleInformation>>();
            returnDataMap.put("result", new ArrayList<VehicleInformation>(vehicleInformationMap.values()));
            return Response.ok(gson.toJson(returnDataMap), MediaType.APPLICATION_JSON).build();
        }catch (VehicleInformationNotFoundException ex) {
            throw ex;
        }
        catch (Throwable t) {
        	throw t;
        }
    }
}
