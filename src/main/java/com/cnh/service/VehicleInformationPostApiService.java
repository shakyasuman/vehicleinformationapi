package com.cnh.service;

import com.cnh.dao.VehicleInformationDao;
import com.cnh.daoImpl.VehicleInformationDaoImpl;
import com.cnh.entity.VehicleInformation;
import com.cnh.enums.Status;
import com.cnh.exceptions.ResponseSuccess;
import com.cnh.exceptions.VehicleInformationServerError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.regex.Pattern;

@Path("save")
public class VehicleInformationPostApiService {
    private static VehicleInformationDao vehicleInformationDaoImpl = new VehicleInformationDaoImpl();

    /**
     *
     * Add new instance of {@link VehicleInformation} in the list {@see VehicleInformationDaoImpl#vehicleInformationList}
     * @return list of available vehicles in {@see VehicleInformationDaoImpl#vehicleInformationList}
     * @param body Request body
     * @throws JSONException
     * @throws VehicleInformationServerError
     * Execute with http://localhost:8080/VehicleInformationApi/api/save?vinNumber=100ABC&make=TEST&year=2001&model=2001&status=Returned
     * @throws ResponseSuccess 
     *
     */
    @POST
    @Path("")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response save(String body) throws JsonParseException, VehicleInformationServerError, ResponseSuccess {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            JSONObject jsonObject = new JSONObject(body);
            Pattern p = Pattern.compile("\\S*(\\S*([a-zA-Z]\\S*[0-9])|([0-9]\\S*[a-zA-Z]))\\S*");

            VehicleInformation vehicleInformation = gson.fromJson(body,VehicleInformation.class);

            VehicleInformation vehicleInformationToBeSaved = new VehicleInformation();

            if (vehicleInformation.getVinNumber() != null && p.matcher(vehicleInformation.getVinNumber()).matches()){
                VehicleInformation result = (VehicleInformation) vehicleInformationDaoImpl.findByVinNumber(vehicleInformation.getVinNumber());
                if (result == null){
                    vehicleInformationToBeSaved.setVinNumber(vehicleInformation.getVinNumber());
                } else {
                    throw new VehicleInformationServerError("Vin Number has been taken.");
                }
            } else if (vehicleInformation.getVinNumber() != null && !p.matcher(vehicleInformation.getVinNumber()).matches()) {
                throw new VehicleInformationServerError("Vin Number should only be alphanumeric.");
            } else {
                throw new VehicleInformationServerError("Vin Number is required.");
            }

            if (vehicleInformation.getMake() != null){
                vehicleInformation.setMake(vehicleInformation.getMake());
            }else {
                throw new VehicleInformationServerError("Make is required.");
            }

            if (vehicleInformation.getYear() != null){
                vehicleInformationToBeSaved.setYear(vehicleInformation.getYear());
            } else {
                throw new VehicleInformationServerError("Year is required.");
            }

            if (vehicleInformation.getModel() != null){
                vehicleInformationToBeSaved.setModel(vehicleInformation.getModel());
            } else {
                throw new VehicleInformationServerError("Model is required.");
            }
            
            if (!jsonObject.has("status")) {
                throw new VehicleInformationServerError("Status is required. Value of Status is one of the list [Production, Marketing, Sold,Returned, NotAvailable]");
            } else if (jsonObject.get("status") == null) {
                throw new VehicleInformationServerError("Status is required.");
            }
            else {
                if (jsonObject.get("status") !=null && Status.getStatusList().contains(vehicleInformation.getStatus())){
                    vehicleInformation.setStatus(vehicleInformation.getStatus());
                } else if (jsonObject.get("status") !=null && !Status.getStatusList().contains(vehicleInformation.getStatus())){
                    throw new VehicleInformationServerError("Value of Status is one of the list [Production, Marketing, Sold,Returned, NotAvailable]");
                }
            }
            vehicleInformationDaoImpl.save(vehicleInformation);
            throw new ResponseSuccess("Successfully Saved.");
        }
        catch (JsonSyntaxException ex){
            throw new VehicleInformationServerError("Only Numbers are allowed. Error: "+ex.getMessage());
        } catch (JSONException ex) {
            throw new VehicleInformationServerError("Json Exception: "+ex.getMessage());
        } catch (VehicleInformationServerError ex){
            throw ex;
        }catch (Exception e){
        	throw new VehicleInformationServerError("Exception occurred. msg= "+e.getMessage());
        }
    }

}
