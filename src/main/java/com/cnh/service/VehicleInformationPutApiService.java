package com.cnh.service;

import com.cnh.dao.VehicleInformationDao;
import com.cnh.daoImpl.VehicleInformationDaoImpl;
import com.cnh.entity.VehicleInformation;
import com.cnh.enums.Status;
import com.cnh.exceptions.ResponseSuccess;
import com.cnh.exceptions.VehicleInformationNotFoundException;
import com.cnh.exceptions.VehicleInformationServerError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.regex.Pattern;

@Path("update")
public class VehicleInformationPutApiService {
    private static VehicleInformationDao vehicleInformationDaoImpl = new VehicleInformationDaoImpl();

    /**
     *
     * An instance of {@link VehicleInformation} is removed from the list {@see VehicleInformationDaoImpl#vehicleInformationList}
     * @param vinNumber some alphanumeric value to represent as Vehicle Identification Number
     * @param body Request body
     * @return list of available vehicles in {@see VehicleInformationDaoImpl#vehicleInformationList}
     * @throws JSONException
     * Execute with http://localhost:8080/VehicleInformationApi/api/update?vinNumber=100ABC
     * @throws ResponseSuccess 
     *
     */
    @PUT
    @Path("")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response udpate(@QueryParam("vinNumber") String vinNumber, String body) throws JSONException, VehicleInformationNotFoundException, VehicleInformationServerError, ResponseSuccess {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            JSONObject jsonObject = new JSONObject(body);
            Pattern p = Pattern.compile("\\S*(\\S*([a-zA-Z]\\S*[0-9])|([0-9]\\S*[a-zA-Z]))\\S*");
            VehicleInformation newVehicleInformation = gson.fromJson(body,VehicleInformation.class);
            VehicleInformation vehicleInformationToBeEdited = new VehicleInformation();
            if (vinNumber != null && p.matcher(vinNumber).matches()){
                VehicleInformation result = vehicleInformationDaoImpl.findByVinNumber(vinNumber);
                if (result == null){
                    throw new VehicleInformationNotFoundException();
                } else {
                    vehicleInformationToBeEdited = result;
                }
            } else if (vinNumber != null && !p.matcher(vinNumber).matches()) {
                throw new VehicleInformationServerError("Vin Number should only be alphanumeric.");
            } else {
                throw new VehicleInformationServerError("Vin Number is required.");
            }

            if (newVehicleInformation.getVinNumber() != null && p.matcher(newVehicleInformation.getVinNumber()).matches()){
                VehicleInformation result = vehicleInformationDaoImpl.findByVinNumber(newVehicleInformation.getVinNumber());
                if (result != null && !newVehicleInformation.getVinNumber().equals(vehicleInformationToBeEdited.getVinNumber())){
                    throw new VehicleInformationServerError("Vin Number has been taken.");
                }
            } else if (newVehicleInformation.getVinNumber() != null && !p.matcher(newVehicleInformation.getVinNumber()).matches()) {
                throw new VehicleInformationServerError("Vin Number should only be alphanumeric.");
            } else {
            	newVehicleInformation.setVinNumber(vehicleInformationToBeEdited.getVinNumber());
            }

            if (newVehicleInformation.getMake() == null) {
            	newVehicleInformation.setMake(vehicleInformationToBeEdited.getMake());
            }

            if (newVehicleInformation.getYear() == null){
                newVehicleInformation.setYear(vehicleInformationToBeEdited.getYear());
            }

            if (newVehicleInformation.getModel() == null){
            	newVehicleInformation.setModel(vehicleInformationToBeEdited.getModel());
            }

            if (jsonObject.get("status") !=null && !Status.getStatusList().contains(newVehicleInformation.getStatus())){
                throw new VehicleInformationServerError("Value of Status is one of the list [Production, Marketing, Sold,Returned, NotAvailable]");
            }
            else if (jsonObject.get("status") == null){
            	newVehicleInformation.setStatus(vehicleInformationToBeEdited.getStatus());
            }
            vehicleInformationDaoImpl.update(vehicleInformationToBeEdited, newVehicleInformation);
            throw new ResponseSuccess("Successfully Updated.");
        } catch (VehicleInformationNotFoundException ex){
            throw ex;
        }
        catch (JsonSyntaxException ex){
            throw new VehicleInformationServerError("Only Numbers are allowed. Error: "+ex.getMessage());
        } catch (JSONException ex) {
            throw new VehicleInformationServerError("Json Exception: "+ex.getMessage());
        } catch (VehicleInformationServerError ex){
            throw ex;
        }catch (Exception e){
        	throw new VehicleInformationServerError("Exception occured. msg = "+e.getMessage());
        }
    }
}
