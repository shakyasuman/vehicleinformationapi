<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html xmlns:th="https://www.thymeleaf.org">
<head>
	<script src="<c:url value="/resources/js/jquery-3.5.1.min.js"/>"></script>
	<script src="<c:url value="/resources/js/jquery-ui.min.js"/>"></script>
    <script src="<c:url value="/resources/js/app.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/app.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/resources/css/all.min.css"/>" type="text/css">    
	<link rel="stylesheet" href="<c:url value="/resources/css/jquery-ui.css"/>" type="text/css">  
    <title>Welcome</title>
</head>
<body>
   <div class="content">
       <h2>Automobile Datastore Api Management</h2>
       <c:if test = "${not empty msg}">
       	<span style="color: red;"><c:out value="Status: ${msg}"></c:out></span><br/>
       </c:if>
       <button id="add">Add</button>
       <form method="post" action="" onsubmit="return false;" id="loginForm">
       		<table>
       				<tr>
	       				<th>Vin Number</th>
	       				<th>Make</th>
	       				<th>Year Year</th>
	       				<th>Model</th>
	       				<th>Status</th>
	       				<th>Action</th>
       				</tr>
       				<tr>
		   				<td><input name="filter.vinNumber" value="" class="filter"/></td>
		   				<td><input name="filter.make" value="" class="filter"/></td>
		   				<td><input name="filter.year" value="" class="filter"/></td>
		   				<td><input name="filter.model" value="" class="filter"/></td>
		   				<td><input name="filter.status" value=""/></td>
		   				<td></td>
	  				</tr>
	  				<c:forEach var="entry" items="${data}">
		  				<tr>
	       					<td><c:out value="${entry.getVinNumber()}"/></td>
	       					<td><c:out value="${entry.getMake()}"/></td>
	       					<td><c:out value="${entry.getYear()}"/></td>
	       					<td><c:out value="${entry.getModel()}"/></td>
	       					<td><c:out value="${entry.getStatus()}"/></td>
	       					<td>
	       						<a href="#" onclick="editVehicleInformation('${entry.getVinNumber()}')">
	       							<i class="fa fa-edit" title="Edit"></i>
	       						</a>
	       						<span style="padding-left:5px"></span>
       							<a href="delete?vinNumber=${entry.getVinNumber()}" onclick="if (!window.confirm('Are you sure?')) {return false;}">
	       							<i class="fa fa-trash" title="Delete"></i>
       							</a>
	       					</td>
	       				</tr>	
	  				</c:forEach>
       		</table>
       </form>
   
   
   <div id="dialogContent" style="display: none;">
	   <form:form method="post" id="addNewAutoForm" action="create" modelAttribute="vehicleInformation">
	      <table>
	          <tr>
	              <td><form:label path="vinNumber">Vin Number</form:label></td>
	              <td><form:input path="vinNumber"/></td>
	          </tr>
	          <tr>
	              <td><form:label path="make">Make</form:label></td>
	              <td><form:input path="make"/></td>
	          </tr>
	          <tr>
	              <td><form:label path="year">Year</form:label></td>
	              <td><form:input path="year"/></td>
	          </tr>
	          <tr>
	              <td><form:label path="model">Model</form:label></td>
	              <td><form:input path="model"/></td>
	          </tr>
	          <tr>
	              <td><form:label path="status">Status</form:label></td>
	              <td><form:input path="status"/></td>
	          </tr>
	          <tr>
	              <td><form:button id="submit" name="submit">Submit</form:button></td>
	              <td><input type="button" id="cancel" name="cancel" value="Cancel"/></td>
	          </tr>
	      </table>
	  </form:form>
   </div>
</div>
       
    
</body>
</html>