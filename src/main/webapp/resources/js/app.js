$(document).ready(function(){
    $('#login').click(function () {
        $('#loginForm').attr('onsubmit', 'return true;');
    })

    $('#add').on('click', function(){
		$('#addNewAutoForm input[type=text]').val('');
		$('#addNewAutoForm').attr("action", "create");
		$('#vinNumber').removeAttr('readonly');
		$( "#dialogContent" ).dialog({
			title:'Add Vehicle Meta Data',
			resizable: false,
			modal: true
		});
	});
	
	$("#cancel").on('click', function(){
		$( "#dialogContent" ).dialog('close');
	})
	
	$('.filter').on("keypress", function(e) {
        if (e.keyCode == 13) {
			$.ajax({  
			    type: "POST",  
			    url: "filter", 
			    contentType: "application/json",
			    data: JSON.stringify({selectedReport:name}),  
			    dataType: "text",
			    success: function(response){
			      if(response.status == "SUCCESS"){
			          console.log("SUCCESS");
			      }else{
			            console.log("FAILURE");
			      }       
			    },  
			    error: function(e){  
			      console.log('Error: ' + e);  
			    }  
			  });  
            return false; // prevent the button click from happening
        }
	});
});
function editVehicleInformation(vinNumber){
	 var data = {
	    "vinNumber" : vinNumber
    }
	$.ajax({  
	    type: "POST",  
	    url: "edit", 
	    data: data, 
		dataType: 'json', 
	    success: function(response){
	      if(response.map.status == "success"){
				$('#vinNumber').val(response.map.vehicleInformation.vinNumber);
				$('#make').val(response.map.vehicleInformation.make);
				$('#year').val(response.map.vehicleInformation.year);
				$('#model').val(response.map.vehicleInformation.model);
				$('#status').val(response.map.vehicleInformation.status);
				$('#vinNumber').attr('readonly','readonly');
				$('#addNewAutoForm').attr("action", "saveEdit");
				$( "#dialogContent" ).dialog({
						title:'Add Vehicle Meta Data',
						resizable: false,
						modal: true
					});
	      }else{
	            console.log("FAILURE");
	      }       
	    },  
	    error: function(e){  
	      console.log('Error: ' + e);  
	    }  
  });  
}
