package com.cnh;

import com.cnh.daoImpl.VehicleInformationDaoImpl;
import com.cnh.entity.VehicleInformation;
import com.cnh.enums.Status;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;


public class VehicleInformationDaoImplTest {
    private VehicleInformationDaoImpl vehicleInformationDao;
    @Before
    public void setUp() throws Exception {
        vehicleInformationDao = new VehicleInformationDaoImpl();
    }
    
    @Test
    public void save() {
        VehicleInformation vehicleInformation = new VehicleInformation();
        vehicleInformation.setVinNumber("asdf234");
        vehicleInformation.setModel(2012);
        vehicleInformation.setYear(2012);
        vehicleInformation.setMake("Ford");
        vehicleInformation.setStatus(Status.Production);
        vehicleInformationDao.save(vehicleInformation);
        VehicleInformation vehicleInformation1 = vehicleInformationDao.findByVinNumber("asdf234");
        Assert.assertNotNull(vehicleInformation1);
    }

    @Test
    public void findByVinNumber() {
        String vinNumber="asdf234";
        VehicleInformation vehicleInformation = vehicleInformationDao.findByVinNumber(vinNumber);
        Assert.assertNotNull(vehicleInformation);

        vinNumber="2D4RN4DG6BR75789222TESTTT";
        VehicleInformation vehicleInformation1 = vehicleInformationDao.findByVinNumber(vinNumber);
        Assert.assertNull(vehicleInformation1);

    }

    @Test
    public void findByMake() {
        String make="Ford";
        List<VehicleInformation> vehicleInformation = vehicleInformationDao.findByMake(make);
        Assert.assertNotNull(vehicleInformation);

        make="SOMETESTBRAND";
        List<VehicleInformation> vehicleInformation1 = vehicleInformationDao.findByMake(make);
        Assert.assertTrue(vehicleInformation1.size()==0);
    }

    @Test
    public void findByYear() {
        Integer year=2012;
        List<VehicleInformation> vehicleInformation = vehicleInformationDao.findByYear(year);
        Assert.assertNotNull(vehicleInformation);
    }

    @Test
    public void findByModel() {
        Integer model=2012;
        List<VehicleInformation> vehicleInformation = vehicleInformationDao.findByModel(model);
        Assert.assertNotNull(vehicleInformation);
    }

    @Test
    public void getAll() {
        List<VehicleInformation> vehicleInformationList = vehicleInformationDao.getAll();
        Assert.assertTrue(vehicleInformationList.size()>0);
    }   
}