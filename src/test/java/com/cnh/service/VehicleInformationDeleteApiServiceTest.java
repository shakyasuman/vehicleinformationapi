package com.cnh.service;

import com.jayway.restassured.RestAssured;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class VehicleInformationDeleteApiServiceTest{
    @Before
    public void setUp() throws Exception {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void delete() {
        given().when().delete("/VehicleInformationApi/api/delete?vinNumber=WBAVT73538FZ62053").then().statusCode(200);
        given().when().delete("/VehicleInformationApi/api/delete?vinNumber=WBAVT73538FZ62053TEST").then().body("message",equalTo("Vehicle Information not found."));
        given().when().delete("/VehicleInformationApi/api/delete?vinNumber=2D4RN4DG6BR757892").then().body("message",equalTo("Sold Vehicle cannot be deleted."));
        given().when().delete("/VehicleInformationApi/api/delete?vinNumber=1C3BC41D8CG189151").then().body("message",equalTo("Marketing Vehicle cannot be deleted."));
    }
}