package com.cnh.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.jayway.restassured.RestAssured;


import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class VehicleInformationGetApiServiceTest {
    @Before
    public void setUp() throws Exception {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getData() {
        given().when().get("/VehicleInformationApi/api/vehicleInformation").then().statusCode(200);
    }

    @Test
    public void getData1() {
        given().when().get("/VehicleInformationApi/api/vehicleInformation/query?vinNumber=2D4RN4DG6BR757892").then().statusCode(200);

        given().when().get("/VehicleInformationApi/api/vehicleInformation/query?vinNumber=3333").then().body("message",equalTo("Vin Number should only be alphanumeric."));

        given().when().get("/VehicleInformationApi/api/vehicleInformation/query?vinNumber=2D4RN4DG6BR757892Test").then().statusCode(404);
    }
}
