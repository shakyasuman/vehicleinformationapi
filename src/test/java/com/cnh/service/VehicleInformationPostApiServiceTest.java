package com.cnh.service;

import com.cnh.entity.VehicleInformation;
import com.cnh.enums.Status;
import com.jayway.restassured.RestAssured;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class VehicleInformationPostApiServiceTest {

    @Before
    public void setUp() throws Exception {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void save() {
        String vinNumber = UUID.randomUUID().toString().replace("-", "");
        given().contentType("application/json")
                .body(new VehicleInformation(vinNumber, "Ford TEst", 2012, 2012, Status.Sold))
                .when().post("/VehicleInformationApi/api/save").then().statusCode(200);

        given().contentType("application/json")
                .body(new VehicleInformation(null, "Ford TEst", 2012, 2012, Status.Sold))
                .when().post("/VehicleInformationApi/api/save").then().body("message",equalTo("Vin Number is required."));

        given().contentType("application/json")
                .body(new VehicleInformation("123", "Ford TEst", 2012, 2012, Status.Sold))
                .when().post("/VehicleInformationApi/api/save").then().body("message",equalTo("Vin Number should only be alphanumeric."));

        given().contentType("application/json")
                .body(new VehicleInformation("1B3EJ46CXTN176123", "Ford TEst", 2012, 2012, Status.Sold))
                .when().post("/VehicleInformationApi/api/save").then().body("message",equalTo("Vin Number has been taken."));
    }
}