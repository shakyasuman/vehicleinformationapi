package com.cnh.service;

import com.cnh.entity.VehicleInformation;
import com.cnh.enums.Status;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class VehicleInformationPutApiServiceTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void udpate() {
        given().contentType("application/json")
                .body(new VehicleInformation("1B3EJ46CXTN176123", "Test Test", 2020, 2020, Status.NotAvailable))
                .when().put("/VehicleInformationApi/api/update?vinNumber=1B3EJ46CXTN176123").then().statusCode(200);

        given().contentType("application/json")
                .body(new VehicleInformation("1B3EJ46CXTN176123", "Test Test", 2020, 2020, Status.NotAvailable))
                .when().put("/VehicleInformationApi/api/update?vinNumber=1B3EJ46CXTN17612333e334343").then().body("message",equalTo("Vehicle Information not found."));
    }
}